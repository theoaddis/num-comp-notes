import numpy as np
from tqdm import trange

def piSquared(n):
        total = np.float64(0)
        for k in trange(1, n+1):
                total += 1 / pow(k, 2)
        return 6 * total

def piSquared_v2(n):
        total = np.float64(0)
        for k in trange(n, 0, -1):
                total += 1 / pow(k, 2)
        return 6 * total

results = {}
results_v2 = {}

test_range = [6, 7, 8, 9]

for i in test_range:
        print(f"\nCalculating piSquared(10^{i})...\n")
        results[i] = piSquared(pow(10, i))
        print(f"\nCalculating piSquared_v2(10^{i})...\n")
        results_v2[i] = piSquared_v2(pow(10, i))

for i in test_range:
        print(f"piSquared(10^{i})    = {results[i]}")
        print(f"piSquared_v2(10^{i}) = {results_v2[i]}")

f = open("results.txt", "w")
f.write(str(results))
f.close()

f = open("results_v2.txt", "w")
f.write(str(results_v2))
f.close()