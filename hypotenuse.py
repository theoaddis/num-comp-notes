import numpy as np

def hypotenuse(x, y):
    """ 
    4.1 (a) (i)
    Find the hypotenuse of a right angled triangle, given x and y, the length of
    two sides of the triangle.
    """
    x = np.double(x)
    y = np.double(y)
    return np.sqrt(np.square(x) + np.square(y))

x = np.double(1)
y_vals = np.arange(0, 301, 10, dtype=np.double)
results = {}

for y in y_vals:
    results[(x, np.power(10, y))] = hypotenuse(x, np.power(10, y))

print("Original hypotenuse results:", results)


def hypotenuse_v1(x, y):
    """
    4.3 (c) (i)
    Find the hypotenuse of a right angled triangle with an improved formula.
    """
    x = np.double(x)
    y = np.double(y)
    if x > y:
        return np.abs(x) * np.sqrt(1 + np.power(y / x, 2))
    else: # if y >= x:
        return np.abs(y) * np.sqrt(1 + np.power(x / y, 2))

results = {}

for y in y_vals:
    results[(x, np.power(10, y))] = hypotenuse_v1(x, np.power(10, y))

print("Improved hypotenuse reults:", results)
